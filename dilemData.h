/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * \file      dilemData.h
 * \author    Antoine Chavee 
 * \brief     Structure de données contenant les informations des joueurs à stocker puis à envoyer au serveur
 *
 */


#include <stdbool.h>

#ifndef DILEMDATA_H
#define DILEMDATA_H


typedef struct {
    int idPlayer; /*!< Numéro de id  du joueur    */
    int idgame; /*!< Numéro de id de la game      */
    int action ; /*!< Action du joueur 1 pour collaborer et 2 pour trahir   */
    int numPr; /*!< Contient le numéro de protocole        */
    int gameStart;
    int gain; /*!< Indique le gain remporté par les joueurs, celui ci evolue 
               * au fur à mesure du jeu          */
    int key; /*!< La clé pour identifier   */
    int cagnotte;  /*!< Constitue la "banque du joueur"   */
    int round; /*!< Numéro de round  */
    int resultChoice; /*!< Le résultat des choix effectué par les joueurs    */
    
} DilemData;


#endif /* PLAYER_H */




