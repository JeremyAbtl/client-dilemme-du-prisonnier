
/**
 * \file      clientcxnmanager.c
 * \author     Antoine Chavee / Jeremy Abitbol
 * \brief      Permet le communication client-serveur avec notamment l'ouverture de connexion
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <pthread.h>
#include <sys/socket.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <stdbool.h>

#include "clientcxnmanager.h"
#include "dilemData.h"
#include "config.h"

/**
 * déclaration et récupération des variables globales 
 */
DilemData globalDilemdata;
extern char ipAddr;
extern char port;
extern int portInt;

/**
 * \brief Thread permettant au serveur de gérer plusieurs connexions client
 * @param ptr connection_t 
 */
void *threadProcess(void * ptr) {
    char buffer_in[BUFFERSIZE];
    int sockfd = *((int *) ptr);
    int len;
    DilemData dilemdata;

       //Welcome the new client
    printf("Welcome new message serveur \n");
 
    //récupération des données envoyer par le serveur
  
    while ((len = read(sockfd, buffer_in, BUFFERSIZE)) != 0) {  
        
        memcpy(&globalDilemdata,buffer_in,sizeof(DilemData));
     
      //appelle d'une fonction chargeant l'affichage avec les données récupérées
      
   launchGame();

  //affichage des données
 
   printf("display Protocole-----------------------------------------------------------\n");
      printf("idPlayer recu serveur: %d\n", globalDilemdata.idPlayer);
        printf("idGame recu serveur : %d\n", globalDilemdata.idgame);
        printf("Action si 1 trahison et si 0 coopère: %d\n", globalDilemdata.action);
        printf("nbProtocole : %d\n", globalDilemdata.numPr);
        printf("player id : %d \n",globalDilemdata.idPlayer);
        printf("game start %d \n",globalDilemdata.gameStart);
        printf("gain %d \n",globalDilemdata.gain);
        printf("cagnotte %d \n",globalDilemdata.cagnotte);
        printf("round %d \n",globalDilemdata.round);
        printf("----------------------------------------------------------------\n");
       
    }
     
      
    close(sockfd);
    printf("client pthread ended, len=%d\n", len);
}
/**
 * \brief Fonction créant la connection avec le serveur
 */
int open_connection() {
    int sockfd;
    const char addrConf = ipAddr;
    struct sockaddr_in serverAddr;
    
 
    int portConfig = portInt;
    

    
   
    // Create the socket. 
    sockfd = socket(AF_INET, SOCK_STREAM, 0);

    //Configure settings of the server address
    // Address family is Internet 
    serverAddr.sin_family = AF_INET;
    //Set port number, using htons function 
    serverAddr.sin_port = htons(portConfig);
    //Set IP address to localhost
    serverAddr.sin_addr.s_addr = inet_addr("127.0.0.1");

    memset(serverAddr.sin_zero, '\0', sizeof serverAddr.sin_zero);

    //Connect the socket to the server using the address
    if (connect(sockfd, (struct sockaddr *) &serverAddr, sizeof (serverAddr)) != 0) {
        printf("Fail to connect to server");
        exit(-1);
    };

    return sockfd;
}
//startGame

