/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * \file      glade.c
 * \author    Antoine Chavee / Jeremy Abitbol
 * \brief     Contient les fonctions qui permettent d'intéragir avec l'interface graphique Glade
 *
 */

#include <stdio.h>
#include <stdlib.h>

#include<gtk/gtk.h>
#include "dilemData.h"
#include <pthread.h>
#include <gtk-3.0/gtk/gtkwidget.h>


#include "clientcxnmanager.h"
/**
 * récupération des variables globales*
 */
extern GtkBuilder *builder;
extern int sockfd;
extern DilemData globalDilemdata;

/**
 * \brief Fonction évènement "on click" qui envoie les données et la décisions du joueur
 */
void on_clickYes(){
  //définition de l'action 1 = colaborer
     globalDilemdata.action = 1;      
  //protocole d'envoie de données serveur lors d'une action joueur
     globalDilemdata.numPr = 33;
    printf("sending : %i\n", globalDilemdata.action);
    
    write(sockfd, &globalDilemdata, sizeof(DilemData));

}
/** \brief Fonction évènement "on click"
 * qui envoie les données et la décisions du joueur
 */
void onClick_No(){
    //définition de l'action 2 = trahir
     globalDilemdata.action = 2; 
     //protocole d'envoie de données serveur lors d'une action joueur
     globalDilemdata.numPr = 33;

    printf("sending : %i\n", globalDilemdata.action);

    write(sockfd,&globalDilemdata, sizeof(DilemData));
 
     
}
/** 
 * \brief Fonction pour fermer gtk_main
 */
void on_window_main_destroy() {
    printf("quitting\n ");
    exit(-5);
    gtk_main_quit();
}



/**
 * \brief Fonction qui lorsqu'on ferme la fenêtre ferme le programme en arrière-plan
 */
void on_cancel() {
    GtkWidget *message_dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_MODAL,
            GTK_MESSAGE_WARNING,
            GTK_BUTTONS_OK_CANCEL,
            "This action will cause the universe to stop existing.");
    //cf : http://refspecs.linuxbase.org/gtk/2.6/gtk/GtkMessageDialog.html
    //https://developer.gnome.org/gnome-devel-demos/stable/messagedialog.c.html.en
    unsigned int pressed = gtk_dialog_run(GTK_DIALOG(message_dialog));
    if (pressed == GTK_RESPONSE_OK) {
        printf("OK Pressed \n");
        printf("quitting\n ");
        gtk_widget_destroy(message_dialog);
        gtk_main_quit();
    } else {
        printf("CANCEL Pressed \n");
        gtk_widget_destroy(message_dialog);
    }
}
/**
 * \brief Fonction qui permet de gérer l'affichage lorsqu'on reçoit des données du serveur
 */
void launchGame(){
    char txt[100];
    /**récupération des id de glade**/
    GtkLabel *startGame = GTK_LABEL(gtk_builder_get_object(builder,"startGame"));
    GtkLabel *gameChoice = GTK_LABEL(gtk_builder_get_object(builder,"gameChoice"));
    GtkLabel *labelMise = GTK_LABEL(gtk_builder_get_object(builder,"labelMise"));
    GtkLabel *labelGain = GTK_LABEL(gtk_builder_get_object(builder,"labelGain"));
    GtkLabel *labelCagnotte = GTK_LABEL(gtk_builder_get_object(builder,"labelCagnotte"));
    GtkLabel *labelCagnottePlayer = GTK_LABEL(gtk_builder_get_object(builder,"labelCagnottePlayer"));
    GtkLabel *idMise = GTK_LABEL(gtk_builder_get_object(builder,"idMise"));
    GtkLabel *idGain = GTK_LABEL(gtk_builder_get_object(builder,"idGain"));
    GtkButton *btnColab = GTK_BUTTON(gtk_builder_get_object(builder,"btnColab"));
    GtkButton *btnTrah = GTK_BUTTON(gtk_builder_get_object(builder,"btnTrah"));
    GtkLabel *labelRound = GTK_LABEL(gtk_builder_get_object(builder,"labelRound"));
    
    /**
     * en fonction des données reçus on set les labels avec les données qu'on 
     * veut afficher en fonction de si la games peut-être lancer mais aussi en 
     * fonction du choix du joueurs 
     */
    gtk_widget_set_sensitive(btnColab,FALSE);
    gtk_widget_set_sensitive(btnTrah,FALSE);
    
    printf("Launch gamestart : %d",globalDilemdata.gameStart);
            
    if(globalDilemdata.gameStart == 1){
        
     gtk_label_set_text(startGame,(gchar *)"Round:");
     
     gtk_label_set_text(gameChoice,(gchar *)"choix");
        
        /**
         globalDilemdata.resultChoice
         * 1:les deux joueurs on colaborer
         * 2:les deux joueurs on trahis
         * 3:le joueur adverse nous a trahis
         * 4:vous avez trahis le joueur adverse
         * 5:la partie est terminée round > 5
         */

    if(globalDilemdata.resultChoice == 1 ){
        gtk_label_set_text(gameChoice,(gchar *)"vous avez colaborer");
    }
     if(globalDilemdata.resultChoice == 2 ){
        gtk_label_set_text(gameChoice,(gchar *)"vous vous êtes mutuellement trahis");
    }
     if(globalDilemdata.resultChoice == 3 ){
        gtk_label_set_text(gameChoice,(gchar *)"il vous à trahi");
    }
    if(globalDilemdata.resultChoice == 4 ){
        gtk_label_set_text(gameChoice,(gchar *)"vous avez trahi");
    }
     if(globalDilemdata.resultChoice == 5 ){
        gtk_label_set_text(gameChoice,(gchar *)"Partie  terminé");   
       
    }   
     
        
     snprintf(txt,100,"%i",globalDilemdata.cagnotte);   
        
    gtk_label_set_text(labelCagnotte,(gchar *)"cagnotte:")
            ;
    gtk_label_set_text(labelCagnottePlayer,txt);
    
    gtk_label_set_text(labelMise,(gchar *)"50");
    
    snprintf(txt,100,"%i",globalDilemdata.gain);   
    
    gtk_label_set_text(labelGain,txt);
    
    snprintf(txt,100,"%i",globalDilemdata.round);
    
   
    
    gtk_label_set_text(labelRound,txt);
    

    gtk_label_set_text(idMise,(gchar *)"Mise:");
    gtk_label_set_text(idGain,(gchar *)"Gain:");
    gtk_widget_set_sensitive(btnColab,TRUE);
    gtk_widget_set_sensitive(btnTrah,TRUE);
    }
    
    
    
}