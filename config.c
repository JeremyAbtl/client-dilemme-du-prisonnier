/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * \file        config.c
 * \author      Jeremy Abitbol
 * \brief       Permet l'ouverture du fichier config.txt afin de récupérer les 
 * informations concernant les joueurs et l'@ip et le port du serveur
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "config.h"
#define TAILLE_MAX 1000 // Tableau de taille 1000


   //création de variable global pour récupérer les les données dans notre .cfg
 
   char ipAddr[TAILLE_MAX] = ""; 
   char port[TAILLE_MAX] = "";
   char idPlayer[TAILLE_MAX] = ""; 
   char socketPlayer[TAILLE_MAX]= "";
   char gainPlayer[TAILLE_MAX] = "";
   
   int portInt;
   int idPlayerInt;
   int socketintPlayer;
/**
 * \brief      lecture de la configuration du joueur contenu dans le fichier config.txt
 */
void conf_player(){
    
    FILE* fp = NULL;
   
   // Chaîne vide de taille TAILLE_MAX;

   
   //on ouvre le fichier
   fp = fopen("config.cfg", "r+");
    
    if (fp != NULL)
        
    {
        
         // On lit maximum TAILLE_MAX caractères du fichier, on stocke le tout dans "ipAddr"
        fgets(ipAddr, TAILLE_MAX, fp); 
        // On affiche la chaîne contenu dans la variable ipAdrr
        printf("%s", ipAddr); 
        
        fgets(port, TAILLE_MAX, fp); 
        printf("%s", port); 
        
        fgets(idPlayer, TAILLE_MAX, fp); 
        printf("%s", idPlayer); 
        
        fgets(socketPlayer, TAILLE_MAX, fp); 
        printf("%s", socketPlayer); 
        
        fgets(gainPlayer, TAILLE_MAX, fp); 
        printf("%s", gainPlayer);
        
        fclose(fp);
    }
    else
    {
        printf("Impossible d'ouvrir le fichier configServer.txt");
    }
   //convertion des données en int
    portInt = atoi(port);
    idPlayerInt = atoi(idPlayer);
    socketintPlayer = atoi(socketPlayer);

    printf("porConfig %d \n",portInt);
    printf("idInt %d \n",idPlayerInt);
    printf("socketInt %d \n",socketintPlayer);
    
}
