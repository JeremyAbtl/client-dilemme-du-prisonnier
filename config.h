/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * \file        config.h
 * \author      Jeremy Abitbol / Antoine Chavee
 * \brief       Contient les signatures des fonctions compris dans le fichier config.c
 */

#ifndef CONFIG_H
#define CONFIG_H
#define TAILLE_MAX 1000

#ifdef __cplusplus
extern "C" {
#endif

/**
 * \brief      lecture de la configuration du joueur contenu dans le fichier config.txt
 */
void conf_player();


#ifdef __cplusplus
}
#endif

#endif /* CONFIG_H */
