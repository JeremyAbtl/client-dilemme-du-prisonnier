/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * \file      glade.c
 * \author    Antoine Chavee / Jeremy Abitbol
 * \brief     Fichier comprenant le main permettant entre autre de lancer la fenêtre GTK et la 
 * lecture du fichier de configuration
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <pthread.h>
#include <sys/socket.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <stdbool.h>
#include <gtk/gtk.h>

#include "clientcxnmanager.h"
#include "glade.h"
#include "dilemData.h"
#include "config.h"

//récupération des variables global et définition du Builder GTK
 
GtkBuilder *builder = NULL;
int sockfd;
extern DilemData globalDilemdata;
extern int idPlayerInt;


int main(int argc, char** argv) {

    GtkWidget *win;
  
    pthread_t thread;
    
    DilemData dilemdata;
    
    
     //appelle de la fonction config_player pour récupérer nos données dans le fichier cfg
     
     conf_player();
    
     //récupération de la sockfd pour communiquer avec notre serveur*
     
     sockfd = open_connection();
     
     //init du gtk
      
     gtk_init(&argc, &argv);
     builder = gtk_builder_new_from_file("glade/AppWin.glade");
    
     //création de notre thread d'écoute pour recevoir les données de notre serveur
     
      pthread_create(&thread,0,threadProcess,&sockfd);
      pthread_detach(thread);

      
      //envoie de données a notre serveur pour pouvoir créer notre game pour notre premier joueurs
       
     dilemdata.idPlayer = idPlayerInt;
     dilemdata.numPr = 80;
     dilemdata.gain = 0;
     
     printf("numProtocole %i \n",dilemdata.numPr);
   
    //envoie de la connection a notre serveur et donc créer ou rejoindre une game
    
     write(sockfd, &dilemdata, sizeof(DilemData));

    win = GTK_WIDGET(gtk_builder_get_object(builder, "win01"));

    gtk_builder_connect_signals(builder, NULL);
    
    //lancement de l'interface
     
     gtk_widget_show(win);
     gtk_main();
     
    }
    
    



