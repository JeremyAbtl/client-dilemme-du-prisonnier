/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * \file      clientcxnmanager.h
 * \brief     Fichier connection client
 */



#include "sys/socket.h"

#ifndef CLIENTCXNMANAGER_H
#define CLIENTCXNMANAGER_H

typedef struct {
    int sockfd; 
    struct sockaddr address; 
    int addr_len; 
    int index; 
} connection_t;

#define BUFFERSIZE 2048

/**
 * \brief Thread permettant au serveur de gérer plusieurs connexions client
 * @param ptr connection_t 
 */
void *threadProcess(void * ptr);

/**
 * \brief Fonction créant la connection avec le serveur
 */
int open_connection();

#endif /* CLIENTCXNMANAGER_H */

