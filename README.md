# README

# Projet C : Dilemme du Prisonnier

## :notebook_with_decorative_cover: Description 
	

Ce projet consiste en un programme simulant des parties de ***dilemme du prisonnier*** entre 
2 joueurs. 

Les parties seront gérés par un ***serveur*** qui possédera son propre service et les joueurs 
seront sur leur poste ***client***, qui possédera lui aussi son propre service. 

Ces deux services ***communiqueront*** afin d'assurer le bon déroulement de chaque 
parties (choix des joueurs, traitement, affichage des gains/mises/résultats etc...)

## :clipboard: Prérequis
Tout d'abord pour utiliser le programme, vous aurez besoin
d'au minimum :

- D'un serveur
- De deux postes clients

	
###  Comment installer

***Expliquer comment installer le programme***

### Mise en place
Une fois installé, dirigez-vous à l'intérieur du fichier config des clients (config.txt).
Vous trouverez une variable appelée ``ipaddr``.

Indiquez à l'intérieur de cette variable l'@IP du serveur qui gère les parties.

## :page_facing_up: Tester le programme
Faites un premier essai en lançant le service du serveur ainsi que celui de deux autres clients.
Normalement, les clients devraient automatiquement se connecter au serveur (Si ceux-ci peuvent bien communiquer), et vous devriez voir la fenêtre du programme s'afficher sur les deux clients. 

Au niveau des tests, lors de la programmation des tests on été effectué afin de vérifier que les règles étant respectés et que les connexions client-serveur et serveur-client étaient fonctionnels.

## :computer: Fonctionnement
Le fonctionnement du programme se découpe en 2 parties : Cliente et Serveur.

### Partie Cliente 
La partie **cliente** représente ce qu'il se passe sur les postes des joueurs. On y  retrouve l'interface du jeu, qui permettra aux joueurs de choisir par le biais de 2 boutons de **Collaborer** ou de **Trahir**.
De plus, l'interface affichera diverses informations comme les tours et le résultat de chaque tour, selon les choix du joueur avec qui il joue.

---

### Partie Serveur
La partie **serveur**, qui comme son nom l'indique est prise en charge par le serveur, concerne le traitement des données. C'est à dire la connexion des joueurs, la création des parties avec l'attribution des joueurs à l'intérieur de celles-ci grâce à l'ID.

---

### Communication
En terme de **communication**, les clients ne font que se connecter et envoyer le choix des joueurs au serveur.
Le serveur de son côté, s'occupera de traiter les parties selon le choix des joueurs qui lui parvient et renverra les résultats adéquats aux joueurs selon leur choix.

:round_pushpin: <b> Note : Les connexions au serveur, les ID des parties crées et les choix de chaque joueurs sont enregistrés dans un fichier log 'log.txt'
</b>


## :pencil: Accéder au code source

Le code source est présent depuis notre Git. Il se compose de deux projets, un spécifique au client, un autre au serveur.

``Client URL :`` https://gitlab.com/JeremyAbtl/client-dilemme-du-prisonnier.git

``Serveur URL :`` https://gitlab.com/JeremyAbtl/serveur-dilemme-du-prisonnnier.git

Il suffit de cloner le repository en utilisant les adresses ci-dessus. Une fois les dossiers contenant le projet récupérer vous pouvez les ouvrir sur netbeans et lancer le programme.

## Versionning
Version 1.0

### Autheurs
- Antoine Chavée
- Jérémy Abitbol
- Robin Amouret
- Rémi Quemin
