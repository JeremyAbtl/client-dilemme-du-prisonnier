/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * \file        config.h
 * \author      Antoine Chavee / Jeremy Abitbol
 * \brief       Contient les signatures des fonctions compris dans le fichier glade.c
 */

#ifndef GLADE_H
#define GLADE_H


    /** 
     * \brief Fonction pour fermer gtk_main
     */
    void on_window_main_destroy();
    
   /** 
    * \brief Fonction évènement "on click"
    * qui envoie les données et la décisions du joueur
    */
    void onClick_No();
    
   /**
    * \brief Fonction évènement "on click" qui envoie les données et la décisions du joueur
    */
    void on_clickYes();
    
    /**
     * \brief Fonction qui lorsqu'on ferme la fenêtre ferme le programme en arrière-plan
     */
    void on_cancel();
    
   /**
    * \brief Fonction qui permet de gérer l'affichage lorsqu'on reçoit des données du serveur
    */
    void launchGame();





#endif /* GLADE_H */

